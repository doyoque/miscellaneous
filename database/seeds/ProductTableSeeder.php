<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$product = [
    		'meja', 'kursi',
    		'taplak', 'lampu',
    		'keyboard', 'mouse',
    		'wallpapper', 'LAN cable',
    		'router', 'cable'
    	];

    	$faker = Faker::create();
    	foreach(range(1, 20) as $index) {
    		DB::table('products')->insert([
    			'name' => $faker->randomElement($product),
    			'detail' => $faker->paragraph,
    			'created_at' => Carbon::now()
    		]);
    	}
    }
}
