<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
	public $code = 401;

	public function login()
	{
		$error = ['error' => 'unauthorized'];

		$credentials = [
			'email' => request('email'),
			'password' => request('password')
		];

		if (Auth::attempt($credentials)) {
			$success['token'] = Auth::user()->createToken('MyApp')->accessToken;

			return response()->json(['success' => $success]);
		}

		return response()->json([$error, $code]);
	}
	public function register(Request $request)
	{
		$Validator = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email',
			'password' => 'required',
			'c_password' => 'required|same:password'
		]);

		if($Validator->fails()){
			return response()->json(['error' => $Validator->errors()], $code);
		}

		$input = $request->all();
		$input['password'] = bcrypt($input['password']);

		$user = User::create($input);
		$success['token'] = $user->createToken('MyApp')->accessToken;
		$success['name'] = $user->name;

		return response()->json(['success' => $success]);
	}

	public function detail()
	{
		$user = Auth::user();
		return response()->json(['success' => $user]);
	}
}
