<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Product') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>
    <div id="app">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark py-lg-2">
        <router-link to="/" class="navbar-brand">{{ __('Home') }}</router-link>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto"></ul>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item py-lg-2">
              <router-link to="/product" class="nav-link">{{ __('product') }}</router-link>
            </li>
            <li class="nav-item py-lg-2">
              <router-link to="/edit" class="nav-link">{{ __('edit') }}</router-link>
            </li>
            <li class="nav-item py-lg-2">
              <router-link to="/create" class="nav-link">{{ __('create') }}</router-link>
            </li>
            <li class="nav-item py-lg-2">
              <router-link to="/show" class="nav-link">{{ __('show') }}</router-link>
            </li>
            <li class="nav-item py-lg-2">
              <router-link to="/animate" class="nav-link">{{ __('animate') }}</router-link>
            </li>
            <li class="nav-item py-lg-2">
              <router-link to="/login" class="nav-link">{{ __('login') }}</router-link>
            </li>
          </ul>
        </div>
      </nav>
        <div class="py-4">
          <div class="container-fluid">
            <router-view></router-view>
          </div>
        </div>
    </div>
  </body>
</html>