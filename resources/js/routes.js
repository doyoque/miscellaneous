import index from './product/IndexComponent.vue'
import edit from './product/EditComponent.vue'
import create from './product/CreateComponent.vue'
import show from './product/ShowComponent.vue'
import root from './product/RootComponent.vue'
import animate from './product/AnimateComponent.vue'
import login from './user/LoginComponent.vue'

const routes = [
	{ path: '/', component: root, name: 'root-page' },
	{ path: '/product', component: index, name: 'product' },
	{ path: '/edit', component: edit, name: 'edit-product' },
	{ path: '/create', component: create, name: 'create-product' },
	{ path: '/show', component: show, name: 'show-product' },
	{ path: '/animate', component: animate, name: 'animate' },
	{ path: '/login', component: login, name: 'login' }
];

export default routes;