/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import routes from './routes.js';

Vue.use(VueRouter);

/*Vue.component('example-component', require('./components/ExampleComponent.vue'));*/
import Example from './components/ExampleComponent.vue'

//vue-router instance
const router = new VueRouter({
	mode: 'history',
	routes
});

//vue instance
const app = new Vue({
    el: '#app',
    router,
    components: {
    	examplecomponent: Example
    }
});
